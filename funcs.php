<?php
if (isset($_POST['login'])) {
    //создаём подключение
    $link = new mysqli($_POST['auth_url'], $_POST['auth_name'], $_POST['auth_pass'], "flowers");
    if (!$link) {
        die('Ошибка соединения: ' . mysql_error());
    }
    //записываем в сессию имя, пароль, урл и connected
    $_SESSION['connected'] = 1;
    $_SESSION['username'] = $_POST['auth_name'];
    $_SESSION['userpass'] = $_POST['auth_pass'];
    $_SESSION['userurl'] = $_POST['auth_url'];
    $link->close();
}

if (isset($_POST['logout'])) {
    //удаление из сессии имени, пароля и т.д.
    unset($_SESSION['connected']);
    unset($_SESSION['username']);
    unset($_SESSION['userpass']);
    unset($_SESSION['userurl']);
    unset($_SESSION['current_table']);
}

if (isset($_POST['show'])) {
    $_SESSION['current_table']=$_POST['cur_table'];
}

if (isset($_POST['add'])) {
    $mysqli = new mysqli($_SESSION['userurl'], $_SESSION['username'], $_SESSION['userpass'], "flowers");
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    if (!$mysqli->set_charset("utf8")) {
        printf("Ошибка при загрузке набора символов utf8: %s\n", $mysqli->error);
    }
    //получаем из данных запроса имена столбцов
    $headers = array_keys($_POST['row']);
    $insert = "";
    //обходим в цикле каждое название столбца из $headers
    //и обращамся к каждому через $header
    foreach ($headers as $header) {
        //формируем строку 'значение1','значение2',...
        if ($insert!="") {
            $insert = $insert . ", ";
        }
        $insert=$insert."'".$_POST['row'][$header]."'";
    }
    if(!$mysqli->query('INSERT INTO '.$_SESSION['current_table'].'('.implode(",",$headers).') VALUES ('.$insert.');')) {
        echo "Не удалось добавить";
    }
    $mysqli->close();
}

if (isset($_POST['delete'])) {
    $mysqli = new mysqli($_SESSION['userurl'], $_SESSION['username'], $_SESSION['userpass'], "flowers");
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    if (!$mysqli->set_charset("utf8")) {
        printf("Ошибка при загрузке набора символов utf8: %s\n", $mysqli->error);
    }
    $auto_inc = $mysqli->query('SHOW COLUMNS FROM '.$_SESSION['current_table'].' WHERE EXTRA LIKE "%auto_increment%"')->fetch_assoc()['Field'];

    if(!$mysqli->query('DELETE FROM '.$_SESSION['current_table'].' WHERE '.$auto_inc.' = '.$_POST['row'][$auto_inc].';')) {
        echo "Не удалось удалить";
    }
    $mysqli->close();

}

if (isset($_POST['edit'])) {
    $mysqli = new mysqli($_SESSION['userurl'], $_SESSION['username'], $_SESSION['userpass'], "flowers");
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    if (!$mysqli->set_charset("utf8")) {
        printf("Ошибка при загрузке набора символов utf8: %s\n", $mysqli->error);
    }
    $auto_inc = $mysqli->query('SHOW COLUMNS FROM '.$_SESSION['current_table'].' WHERE EXTRA LIKE "%auto_increment%"')->fetch_assoc()['Field'];
    //получаем имена столбцов
    $headers = array_keys($_POST['row']);
    $sets = "";
    $where="";
    for ($i=0;$i<count($headers);$i++) {
        if ($headers[$i]!=$auto_inc) {
            if ($sets!="") $sets=$sets.", ";
            $sets=$sets.$headers[$i]." = '".$_POST['row'][$headers[$i]]."'";
        }
        else {
            $where = $headers[$i]." = ".$_POST['row'][$headers[$i]];
        }
    }
    if(!$mysqli->query('UPDATE '.$_SESSION['current_table'].' SET '.$sets.' WHERE '.$where.";")) {
        echo "Не удалось обновить";
    }
    $mysqli->close();
}
