<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<?php
session_start();
include('funcs.php');

//если в сессии нет connected
if (!isset($_SESSION['connected'])) {
?>
<form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <input type="text" name="auth_url" placeholder="url"><br>
    <input type="text" name="auth_name" placeholder="логин"><br>
    <input type="password" name="auth_pass" placeholder="пароль"><br>
    <input type="submit" name="login">
</form>
<?php
}
else {
    //кнопка выхода
    ?>
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <button type="submit" name="logout">Выйти</button>
    </form>
    <?php

    //создать подключение к бд, используя логин, урл, пароль из сессии
    $mysqli = new mysqli($_SESSION['userurl'], $_SESSION['username'], $_SESSION['userpass'], "flowers");
    if ($mysqli->connect_errno) {
        echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
    }
    else {
        //установка кодировки утф8
        if (!$mysqli->set_charset("utf8")) {
            printf("Ошибка при загрузке набора символов utf8: %s\n", $mysqli->error);
        }
        //получаем список таблиц
        $res = $mysqli->query('SHOW TABLES;');
        //форма "Выберите таблицу" ?>
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <label for="select">Выберите таблицу:</label><select name="cur_table" id="select">';
        <?php while ($row = $res->fetch_assoc()) { //row - одна строка из $res
            echo '<option>'.$row['Tables_in_flowers']."</option>";
        } ?>
        </select>
            <input type="submit" name="show" value="Выбрать">
            </form>
<?php
    }
}

//проверяем есть ли $_SESSION['current_table'] (имя выбранной таблицы)
if (isset($_SESSION['current_table'])) {

    echo "<p>Таблица: <b>".$_SESSION['current_table']."</b></p>";
    //запрос на выборку всех записей из выбранной таблицы
    $res = $mysqli->query('SELECT * FROM '.$_SESSION['current_table']);
    //получаем имя столбца типа счётчик
    $auto_inc = $mysqli->query('SHOW COLUMNS FROM '.$_SESSION['current_table'].' WHERE EXTRA LIKE "%auto_increment%"')->fetch_assoc()['Field'];

    echo '<table border="1" id="tsort" style="border-collapse: collapse"><thead><tr>';
    //получаем имена столбцов из результата ($res)
    $headers = $res->fetch_fields();
    //выводим имена столбцов в шапку
    foreach ($headers as $v) printf('<th> %s </th>', $v->name);

    echo '<th>Действия</th></tr></thead><tbody>';

    //row - одна строка из $res
    //для каждой строки делаем форму из текстовых полей
    //и заполняем их полученными данными из $res
    while ($row = $res->fetch_array(MYSQLI_NUM)) {
        echo '<tr><form method="POST" action="'.$_SERVER["PHP_SELF"].'">';
        $counter=0;
        //перебираем значения столбцов в заданной строке и каждый
        //заносим в таблицу в виде текстового поля
        foreach ($row as $f) {
            echo "<td><input type='text' name='row[{$headers[$counter]->name}]' value='{$f}'";
            //echo "<td><input type='text' name='row[{$counter}][{$headers[$counter]->name}]' value='{$f}'";
            //если столбец типа "счётчик", то делаем данное текстовое поле недоступным для редактирования (readonly)
            if ($headers[$counter]->name == $auto_inc) {
                echo " readonly ";
            }
            echo "></td>";
            $counter++;
        }
        echo '<td><input type="submit" name="edit" value="Редактировать">
                  <input type="submit" name="delete" value="Удалить"></td></form></tr>';
    }
    //добавление строки для ввода данных
    echo '<tr><form method="POST" action="'.$_SERVER["PHP_SELF"].'">';

    for ($i =0;$i<$res->field_count;$i++) {
        //если столбец не счётчик, то выводим текстовое поле
        if ($headers[$i]->name != $auto_inc) {
            echo '<td><input type="text" name="row['.$headers[$i]->name.']"></td>';
        }
        else {
            echo '<td></td>';
        }
    }
    echo '<td><input type="submit" name="add" value="Добавить"></td></form></tr></tbody></table>';
}
?>

